#!/usr/bin/env python3

import logging as log
from PIL import Image, ImageDraw, ImageFont
from os.path import exists
import urllib.request
import matplotlib.pyplot as plt


def gen_log(filename, headers, df, df_old, font, header_font, size, col_margin, row_margin, text_color,
            background_color):
    up_icon = Image.open('icons/Arrow_UP.png').resize((size, size))
    down_icon = Image.open('icons/Arrow_DOWN.png').resize((size, size))

    hfnt = ImageFont.truetype(header_font, size)
    header_widths = list(map(lambda x: x[0], map(hfnt.getsize, headers)))
    header_height = max(map(lambda x: x[1], (map(hfnt.getsize, headers))))

    fnt = ImageFont.truetype(font, size)
    player_width = max(map(lambda x: x[0], (map(fnt.getsize, df.display_name))))
    player_height = max(map(lambda x: x[1], (map(fnt.getsize, df.display_name))))

    sent_width = max(map(lambda x: x[0], (map(fnt.getsize, [str(x) for x in df.sentiment]))))
    icon_width = int(size * 2)
    sent_change_width = fnt.getsize("-10")[0]
    pick_width = max(map(lambda x: x[0], (map(fnt.getsize, [str(int(x)) for x in df.selection]))))
    price_width = max(map(lambda x: x[0], (map(fnt.getsize, [str(x) for x in df.price]))))

    cols = [col_margin]
    cols.append(cols[0] + col_margin + max(header_widths[0], player_width))
    cols.append(cols[1] + col_margin + max(header_widths[1], sent_width + icon_width + sent_change_width))
    cols.append(cols[2] + col_margin + max(header_widths[2], pick_width))
    cols.append(cols[3] + col_margin + max(header_widths[2], price_width))

    rows = [header_height + row_margin + i * player_height for i in range(len(df))]

    img = Image.new('RGBA', (cols[4], player_height * len(df) + header_height + row_margin), color=background_color)
    d = ImageDraw.Draw(img)

    # Draw header
    d.text((cols[0], 0), headers[0], fill=text_color, font=hfnt)
    d.text((cols[1], 0), headers[1], fill=text_color, font=hfnt)
    d.text((cols[2], 0), headers[2], fill=text_color, font=hfnt)
    d.text((cols[3], 0), headers[3], fill=text_color, font=hfnt)

    for i, (index, row) in enumerate(df.iterrows()):
        # Name
        d.text((cols[0], rows[i]), row.display_name, fill=text_color, font=fnt)
        # Sentiment
        color = 'green' if row.sentiment >= 0 else 'red'
        w = sent_width - fnt.getsize(str(row.sentiment))[0]
        d.text((cols[1] + w, rows[i]), str(row.sentiment), fill=color, font=fnt)
        diff = row.sentiment - df_old.loc[index, 'sentiment']
        if diff != 0:
            icon = up_icon if diff >= 0 else down_icon
            img.alpha_composite(icon, (cols[1] + sent_width + int(size / 2), rows[i] + int(player_height / 4)))
            color = 'green' if diff >= 0 else 'red'
            d.text((cols[1] + sent_width + icon_width, rows[i]), str(abs(diff)), fill=color, font=fnt)

        # Pick %
        w = fnt.getsize(str(int(row.selection)))[0] + col_margin
        d.text((cols[3] - w, rows[i]), str(int(row.selection)), fill=text_color, font=fnt)
        # Price
        w = fnt.getsize(str(row.price))[0] + col_margin + 10
        d.text((cols[4] - w, rows[i]), str(row.price), fill=text_color, font=fnt)

    img.save(filename)


def gen_plot(history, index):
    x = history.time
    y = [x.loc[index, 'sentiment'] for x in history.data]
    plt.plot(x, y)
    plt.ylim([-100, 100])
    plt.xticks([])
    filename = 'sentiment_plot.png'
    plt.savefig(filename)
    return filename


team_color = {'ALF': '#B12039',
              'ALP': '#2293D1',
              'ALT': '#4E7C9B',
              'AST': '#2D826D',
              'FER': '#ED1C24',
              'HAA': '#B6BABD',
              'MCL': '#F58020',
              'MER': '#6CD3BF',
              'RED': '#1E5BC6',
              'WIL': '#37BEDD'}


# If price is true, do a price change notification
# Else, do a sentiment notification
def gen_notify(price, player_id, icons_path, df, df_old):
    team_font = 'fonts/Kanit-Bold.ttf'
    driver_font = 'fonts/Kanit-SemiBold.ttf'

    img = Image.open(f'{icons_path}/Alert_Template.png')
    d = ImageDraw.Draw(img)

    t_fnt = ImageFont.truetype(team_font, 16)
    d_fnt = ImageFont.truetype(driver_font, 17)

    # Paste Picture
    pic_filename = f"{icons_path}/{df.loc[player_id, 'display_name']}.png"
    if not exists(pic_filename):
        urllib.request.urlretrieve(df.loc[player_id, 'pic_url'], pic_filename)

    pic = Image.open(pic_filename).resize((45, 45)).convert('RGBA')
    img.alpha_composite(pic, (3, 5))

    # Team Name
    d.rounded_rectangle((53, 3, 91, 23), radius=4, fill=team_color[df.loc[player_id, 'team']])
    d.text((56, 1), df.loc[player_id, 'team'], fill='white', font=t_fnt)

    # Player Name
    name = df.loc[player_id, 'display_name'].upper()
    if df.loc[player_id, 'type'] == "Driver":
        name = name[3:]
    d.text((98, 0), name, fill='black', font=d_fnt)

    if price:
        # Price up/down symbol
        if df.loc[player_id, 'price'] - df_old.loc[player_id, 'price'] >= 0:
            direction = "up"
            icon = Image.open(f'{icons_path}/Arrow_UP_Centered.png').resize((25, 25))
        else:
            direction = "down"
            icon = Image.open(f'{icons_path}/Arrow_DOWN_Centered.png').resize((25, 25))
        img.alpha_composite(icon, (60, 23))
    else:
        # Sentiment
        if df.loc[player_id, 'sentiment'] >= 0:
            color = 'green'
            s = str(df.loc[player_id, 'sentiment'])
        else:
            color = 'red'
            s = str(df.loc[player_id, 'sentiment'])[1:]
        d.text((56, 23), f"{s}%", fill=color, font=t_fnt)

    # Current Price
    if not price or df.loc[player_id, 'price'] == df_old.loc[player_id, 'price']:
        color = 'black'
    else:
        color = 'green' if df.loc[player_id, 'price'] - df_old.loc[player_id, 'price'] >= 0 else 'red'
    d.text((98, 23), f"${df.loc[player_id, 'price']}M", fill=color, font=d_fnt)

    if price:
        filename = f"{df.loc[player_id, 'display_name']} price {direction} ${df.loc[player_id, 'price']}M.png"
    else:
        filename = f"{df.loc[player_id, 'display_name']} sentiment {df.loc[player_id, 'sentiment']}%.png"
    img.save(filename)
    return filename
