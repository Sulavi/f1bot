Build Docker Image:
 - docker build -t f1bot .
 
Run Docker Image:
 - docker run -d --restart unless-stopped --mount type=bind,source="/home/richard/Projects/Discord Bots/F1Production/logs",target=/bot/logs --mount type=bind,source="/home/richard/Projects/Discord Bots/F1Production/config",target=/bot/config --name f1 f1bot
