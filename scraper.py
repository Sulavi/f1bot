import logging as log
from requests import request
import json


# Scrapes the latest data, return it or None
async def scrape_data(url):
    try:
        response = request("GET", url, headers={}, data={})
        data = json.loads(response.text)
        if 'errors' in data:
            log.error(f'Error in json: {data}')
            return None
        return data
    except Exception as err:
        log.error(f'Unable to scrape data: {err}')
        return None
