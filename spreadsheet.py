from openpyxl import Workbook
from openpyxl.reader.excel import load_workbook
import os.path

import unittest


class Spreadsheet:
    def write_cell(self, sheet, cell, value, style=None):
        pass

    def read_cell(self, sheet, cell):
        pass


class ExcelSheet(Spreadsheet):
    def __init__(self, filepath):
        self.filepath = filepath
        if os.path.isfile(self.filepath):
            self.wb = load_workbook(filename=self.filepath)
        else:
            self.wb = Workbook()

    def __del__(self):
        self.wb.close()

    def write_cell(self, sheet, cell, value, style=None):
        if sheet not in self.wb.worksheets:
            self.wb.create_sheet(sheet)
        self.wb[sheet][cell] = value
        self.wb.save(filename=self.filepath)

    def read_cell(self, sheet, cell):
        return self.wb[sheet][cell].value


class TestExcelSpreadsheet(unittest.TestCase):
    def setUp(self):
        self.wb = ExcelSheet("testsheet.xlsx")

    def tearDown(self):
        pass
        if os.path.exists("testsheet.xlsx"):
            os.remove("testsheet.xlsx")

    def test_write_to_cell(self):
        self.wb.write_cell("sheet 1", "A1", "Test")
        value = self.wb.read_cell("sheet 1", "A1")
        self.assertEqual(value, "Test")
        self.wb.write_cell("sheet 1", "A2", 158.6)
        value = self.wb.read_cell("sheet 1", "A2")
        self.assertEqual(value, 158.6)


if __name__ == '__main__':
    unittest.main()

