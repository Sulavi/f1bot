FROM python:latest
RUN pip install --no-cache-dir -U \
    discord \
    discord-interactions \
    discord-py-interactions \
    discord-py-slash-command \
    Pillow \
    requests \
    pandas \
    matplotlib
WORKDIR /bot
COPY . .
CMD ["python3", "f1bot.py"]