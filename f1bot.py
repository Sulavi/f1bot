#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 2022

@author: richard
"""

import asyncio

import discord
import json
import argparse
import glob
import os
import queue

from datetime import datetime
from enum import Enum
from os.path import exists, basename

import logging as log
from logging.handlers import TimedRotatingFileHandler, QueueHandler
import pandas as pd

from functools import reduce

from discord import Client, Intents, ChannelType
from discord.ext import commands
from discord_slash import SlashCommand
from discord.utils import get

from picture_gen import gen_log, gen_notify
from scraper import scrape_data

# bot_id = 953254984877748265
url = "https://fantasy-api.formula1.com/f1/2022/players"
log_path = '.'

# Setup bot
bot = commands.Bot(command_prefix="!", self_bot=True, intents=Intents.default())
slash = SlashCommand(bot, sync_commands=True)


class ChannelType(Enum):
    DEBUG = 1  # Debug info
    LOGGING = 2  # All changes to the data
    NOTIFY = 3  # Anonymous notification if sentiment changes threshold
    PREMIUM = 4  # Specific notification if sentiment changes threshold


channels = {ChannelType.DEBUG: [],
            ChannelType.LOGGING: [],
            ChannelType.NOTIFY: [],
            ChannelType.PREMIUM: []}


def save_json(json_data, path):
    date = datetime.now().strftime("%m-%d_%H:%M:%S")
    filepath = f"{path}/{date}.json"
    log.debug(f'Saving to file {filepath}')
    with open(filepath, 'w') as f:
        json.dump(json_data, f, indent=3)


def df_hash(df):
    return reduce(lambda x, y: x ^ y, pd.util.hash_pandas_object(df))


def json_to_pandas(json_data):
    sentiment = []
    for player in json_data['players']:
        s = player['current_price_change_info']['probability_price_up_percentage'] - \
            player['current_price_change_info']['probability_price_down_percentage']
        sentiment.append(s)

    df = pd.DataFrame(
        {
            "id": [player['id'] for player in json_data['players']],
            "type": [player['position'] for player in json_data['players']],
            "display_name": [player['display_name'] for player in json_data['players']],
            "sentiment": sentiment,
            "selection": [player['current_price_change_info']['current_selection_percentage'] for player in
                          json_data['players']],
            "price": [player['price'] for player in json_data['players']],
            "pic_url": [player['headshot']['player_list'] for player in json_data['players']],
            "team": [player['team_abbreviation'] for player in json_data['players']]
        },
        index=[player['id'] for player in json_data['players']]
    )
    df = df.sort_values(by='sentiment', ascending=False)
    return df


# Loads up to the past 24 hours of data
def load_history(path):
    history = pd.DataFrame(columns=['time', 'data'])
    files = sorted(glob.glob(f'{path}/*.json'), reverse=True)
    for file in files:
        timestamp = basename(file)[:-5]
        dt = datetime.strptime(timestamp, "%m-%d_%H:%M:%S").replace(year=datetime.now().year)
        # If timestamp is less than 24 hours ago, add to history
        if (datetime.now() - dt).days == 0:
            with open(file, 'r') as f:
                data = json_to_pandas(json.load(f))
            history = history.append({'time': dt, 'data': data}, ignore_index=True)
        else:
            break
    return history


def add_to_history(history, df):
    new_df = pd.DataFrame([{'time': datetime.now(), 'data': df}])
    history = new_df.append(history, ignore_index=True)
    return history[history.apply(lambda row: (datetime.now() - row.time).days == 0, axis=1)]


def wait_time(df):
    max_sent = reduce(max, map(abs, df.sentiment))
    if max_sent > 90:
        return 60 * 3
    else:
        return 60 * 10


def get_player(id, json_data):
    for player in json_data['players']:
        if player['id'] == id:
            return player
    return None


def get_sentiment(player):
    return (player['current_price_change_info']['probability_price_up_percentage'] -
            player['current_price_change_info']['probability_price_down_percentage'])


def get_level(sentiment):
    s = abs(sentiment)
    level = 0
    if s >= 80:
        level = 80
    if s >= 85:
        level = 85
    if s >= 90:
        level = s
    if sentiment < 0:
        level *= -1
    return level


def get_level_changed(df, df_old):
    ids = []
    for index, row in df.iterrows():
        level = get_level(row['sentiment'])
        old_level = get_level(df_old.loc[index, 'sentiment'])
        if level != old_level:
            if level != 0:
                ids.append(index)
    return ids


def get_price_changed(df, df_old):
    ids = []
    for index, row in df.iterrows():
        price_change = row['price'] - df_old.loc[index, 'price']
        if price_change != 0:
            ids.append(index)
    return ids


def clear_files(filenames):
    for file in filenames:
        try:
            os.remove(file)
        except Exception as err:
            log.error(f'Unable to remove file {file}: {err}')


async def send_notify(history):
    df = history.data[0]
    df_old = history.data[1]
    notify_ids = get_level_changed(df, df_old)
    notify_files = [gen_notify(False, i, 'icons', df, df_old) for i in notify_ids]
    log.info(f"notify_files {notify_files}")
    price_change_ids = get_price_changed(df, df_old)
    price_change_files = [gen_notify(True, i, 'icons', df, df_old) for i in price_change_ids]
    log.info(f"price_change_files {price_change_files}")

    for ch in channels[ChannelType.NOTIFY]:
        try:
            channel = bot.get_channel(ch)
            if channel is not None:
                for file in price_change_files:
                    await channel.send(file=discord.File(file))

                for file in notify_files:
                    await channel.send(file=discord.File(file))
            else:
                log.warning(f"Unable to get channel id {ch}")
        except Exception as e:
            log.error(f'Unable to send notify message {e}')

    for ch in channels[ChannelType.PREMIUM]:
        try:
            channel = bot.get_channel(ch)
            if channel is not None:
                pass
                # await channel.send(premium_notify)
            else:
                log.warning(f"Unable to get channel id {ch}")
        except Exception as e:
            log.error(f'Unable to send premium message {e}')

    clear_files(notify_files + price_change_files)


async def send_log(history):
    df = history.data[0]
    df_old = history.data[1]
    gen_log('drivers.png', ['Driver', 'Sentiment', 'Pick %', 'Price'],
            df[df['type'] == "Driver"], df_old[df_old['type'] == "Driver"],
            font='fonts/Kanit-Light.ttf',
            header_font='fonts/Kanit-Medium.ttf',
            size=20,
            col_margin=25,
            row_margin=5,
            text_color='black',
            background_color='white')

    gen_log('constructors.png', ['Constructor', 'Sentiment', 'Pick %', 'Price'],
            df[df['type'] == "Constructor"], df_old[df_old['type'] == "Constructor"],
            font='fonts/Kanit-Light.ttf',
            header_font='fonts/Kanit-Medium.ttf',
            size=20,
            col_margin=25,
            row_margin=5,
            text_color='black',
            background_color='white')

    try:
        for ch in channels[ChannelType.LOGGING]:
            channel = bot.get_channel(ch)
            if channel is not None:
                await channel.send(file=discord.File('drivers.png'))
                await channel.send(file=discord.File('constructors.png'))
            else:
                log.warning(f"Unable to get channel id {ch}")
    except Exception as e:
        log.error(f'Unable to send log message {e}')


async def poll_api(json_path):
    # Load previous data from files
    history = load_history(json_path)

    if len(history.index) > 0:
        pd_hash = df_hash(history.data[0])
        log.info(f"Loaded {len(history.index)} json data from file")
    else:
        pd_hash = 0
        log.info("Unable to load from file")

    while True:
        await bot.wait_until_ready()
        new_json_data = await scrape_data(url)

        if new_json_data is not None:
            new_pd = json_to_pandas(new_json_data)
            new_hash = df_hash(new_pd)
            if new_hash == pd_hash:
                log.info(f"Polled data with the same hash {hex(new_hash).upper()[2:]}")
            else:
                log.info(f'Polled new data with hash {hex(new_hash).upper()[2:]}')

                history = add_to_history(history, new_pd)
                log.debug(f"History size is {len(history)}")

                if len(history.index) >= 2:
                    await send_log(history)
                    await send_notify(history)

                pd_hash = new_hash
                save_json(new_json_data, json_path)

        # Sleep before next poll
        poll_time = wait_time(history.data[0])
        await asyncio.sleep(poll_time)


async def send_discord_logs(debug_channels, message_queue):
    await bot.wait_until_ready()
    while True:
        while not message_queue.empty():
            msg = message_queue.get_nowait()
            for ch in debug_channels:
                channel = bot.get_channel(ch)
                if channel is not None:
                    await channel.send(msg)
                else:
                    log.warning(f"Unable to get channel id {ch}")
        await asyncio.sleep(5)


def main():
    global channels
    global log_path

    parser = argparse.ArgumentParser(description='Runs the ORC Alert discord bot')
    parser.add_argument('--secret', '-s', action='store', nargs='?',
                        default='config/secret.txt',
                        help='the bot token as a filepath, default secret.txt')

    parser.add_argument('--channels', '-c', action='store', nargs='?',
                        default='config/channels.json',
                        help='a file with the channel names and type')

    parser.add_argument('--log', '-l', action='store', nargs='?',
                        default='logs/F1Bot.log',
                        help='filepath for the log, default logs/F1Bot.log')

    parser.add_argument('--log_level', '-v', action='store', nargs='?',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], default='INFO',
                        help='level of detail for the console output; default INFO')

    parser.add_argument('--json_path', '-p', action='store', nargs='?',
                        default='logs',
                        help='path to save json data to, default logs')

    parser.add_argument('--excel_path', '-x', action='store', nargs='?',
                        default='sheet',
                        help='path to spreadsheet')

    args = parser.parse_args()

    # Set up logging
    root_logger = log.getLogger()
    root_logger.setLevel('DEBUG')
    log_formatter = log.Formatter('%(asctime)s [%(levelname)-5.5s] %(message)s')
    file_handler = TimedRotatingFileHandler(args.log, when='W6', backupCount=10)
    file_handler.setFormatter(log_formatter)
    file_handler.setLevel(getattr(log, args.log_level))
    root_logger.addHandler(file_handler)

    console_handler = log.StreamHandler()
    console_handler.setFormatter(log_formatter)
    console_handler.setLevel(getattr(log, args.log_level))
    root_logger.addHandler(console_handler)

    message_queue = queue.SimpleQueue()
    discord_handler = QueueHandler(message_queue)
    discord_formatter = log.Formatter('This is a test %(message)s')
    discord_handler.setFormatter(discord_formatter)
    discord_handler.setLevel('ERROR')
    root_logger.addHandler(discord_handler)

    log.info(f"Bot Started, {args}")

    try:
        with open(args.channels, 'r') as f:
            chnls = json.loads(f.read())
            for channel in chnls['channels']:
                channels[ChannelType[channel['type']]].append(channel['id'])
    except Exception as e:
        log.critical(f"Unable to load channels file: {e}")

    if exists(args.secret):
        log.debug(f'Using contents of file as secret')
        with open(args.secret, "r") as f:
            secret = f.read()
    else:
        log.critical(
            "Unable to start without valid bot token. Pass in a file with -s <filepath> or use the default secret.txt")
        return

    bot.loop.create_task(poll_api(args.json_path))
    bot.loop.create_task(send_discord_logs(channels[ChannelType.DEBUG], message_queue))
    bot.run(secret)


if __name__ == "__main__":
    main()
